<?php
$accountsummary->insert_configuration();
$message = '';
if (isset($_POST['savechanges'])) {
   $message =  $accountsummary->update_configuration($_POST);
}
$data = $accountsummary->accountStatement_config();
?>
<div class="alert alert-warning text-center">
    <div class="input-group">
        <span class="input-group-addon"><?php echo $LANG['linktemplateclients'];?></span>
        <input type="text" id="cronPhp" value="<?php echo $baseurl . "/index.php?m=accountStatement"; ?>" class="form-control">
    </div>
</div>
<?php if(!empty($message)){ ?>
<div class="errorbox">
    <strong><span class="title">Error</span></strong>
    <br><?php echo $message; ?>
</div>
<?php  } ?>
<form method="post"enctype="multipart/form-data">
    <table class="table table-striped table-bordered table-condensed table-config">
        <tr>
            <th><?php echo $LANG['pdfpapersize'];?></th>
            <td>
                <select name="pdfpapersize" class="form-control select-inline">
                    <option value="A4"<?php if ($data['PDFPaperSize'] == 'A4') { ?> selected="selected"<?php } ?>>A4</option>
                    <option value="Letter"<?php if ($data['PDFPaperSize'] == 'Letter') { ?> selected="selected"<?php } ?>><?php echo $LANG['pdfletter'];?></option>
                </select>
               <?php echo $LANG['choosegeneratingfiles'];?>
            </td>
        </tr>
        <tr>
            <th><?php echo $LANG['pdffontfamily'];?></th>
            <td>
                <label class="radio-inline" style="float: left;">
                    <input type="radio" name="tcpdffont" value="courier"<?php if ($data['TCPDFFont'] == 'courier') { ?> checked="checked"<?php } ?>> Courier
                </label>
                <label class="radio-inline" style="float: left;">
                    <input type="radio" name="tcpdffont" value="freesans"<?php if ($data['TCPDFFont'] == 'freesans') { ?> checked="checked"<?php } ?>> Freesans
                </label>
                <label class="radio-inline" style="float: left;">
                    <input type="radio" name="tcpdffont" value="helvetica"<?php if ($data['TCPDFFont'] == 'helvetica') { ?> checked="checked"<?php } ?>> Helvetica
                </label>
                <label class="radio-inline" style="float: left;">
                    <input type="radio" name="tcpdffont" value="times"<?php if ($data['TCPDFFont'] == 'times') { ?> checked="checked"<?php } ?>> Times
                </label> 
                <label class="radio-inline" style="float: left;">
                    <input type="radio" name="tcpdffont" id="tcpdffontCustomRd" value="custom"<?php if ($data['TCPDFFont'] == 'custom') { ?> checked="checked"<?php } ?>> Custom &nbsp; &nbsp; &nbsp;
                </label>
<!--                <input type="text" name="tcpdffontcustom" size="15" class="input-type-inline" value="">-->
                <input type="file" onclick="$('#tcpdffontCustomRd').prop('checked',true);" name="tcpdffontcustom"  style="float: left;" /><?php echo $data['TCPDFCoustomFont']; ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $LANG['invoicetype'];?></th>
            <td>
                <select name="pdfinvoicetype" class="form-control select-inline">
                    <option value="unpaid"<?php if ($data['PDFInvoiceType'] == "unpaid") { ?> selected="selected"<?php } ?>><?php echo $LANG['invoicesunpaid'];?></option>
                    <option value="paid"<?php if ($data['PDFInvoiceType'] == "paid") { ?> selected="selected"<?php } ?>><?php echo $LANG['invoicespaid'];?></option>
                    <option value="all"<?php if ($data['PDFInvoiceType'] == "all") { ?> selected="selected"<?php } ?>><?php echo $LANG['invoicesall'];?></option>
                </select>
                <?php echo $LANG['chooseinvoicegeneratingfiles'];?>
            </td>
        </tr>
        <tr>
            <th><?php echo $LANG['enableinvoices'];?></th>
            <td>
                <label class="checkbox-inline">
                    <input type="checkbox" name="enablepdfinvoices"<?php if ($data['EnablePDFInvoices'] == 'on') { ?> checked="checked"<?php } ?>> <?php echo $LANG['ttspdf'];?>
                </label>                
            </td>
        </tr>
        <tr>
            <th><?php echo $LANG['enableautomatic'];?></th>
            <td>
                <label class="checkbox-inline">
                    <input type="checkbox" name="enableautostatement" <?php if ($data['EnableAutoStatement'] == 'on') { ?> checked="checked" <?php } ?>> <?php echo $LANG['ttsmonthlystatement'];?>
                </label>                
            </td>
        </tr>
        <tr>
            <th><?php echo $LANG['includeallpaidinvoices'];?></th>
            <td>
                <label class="checkbox-inline">
                    <input type="checkbox" name="includeallunpaidinvoices" <?php if ($data['IncludeUnpaidInvoices'] == 'on') { ?> checked="checked" <?php } ?>> <?php echo $LANG['includeallpaidinvoicesdes'];?>
                </label>                
            </td>
        </tr>
    </table>
    <p align="center" style="margin: 15px;">
        <input type="submit" name="savechanges" value="Save Changes" class="btn btn-success my-button-country configuration-btn">
    </p>
</form>