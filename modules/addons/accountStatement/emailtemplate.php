<?php
    if(isset($_POST['savechanges'])){
        $accountsummary->addEmailTemplates($_POST);
    }
    $data = $accountsummary->getEmailTemplates();
?>
<form method="post">
    <table class="form" width="100%">
        <tr>
            <td class="fieldlabel"><?php echo $LANG['from'];?></td>
            <td class="fieldarea">
                <input type="text" name="fromname" size="25" value="<?php echo $data['fromname']; ?>" data-enter-submit="true" placeholder="Name">
                <input type="text" name="fromemail" size="40" value="<?php echo $data['fromemail']; ?>" data-enter-submit="true" placeholder="email">
            </td>
        </tr>
        <tr>
            <td class="fieldlabel"><?php echo $LANG['copyto'];?></td>
            <td class="fieldarea">
                <input type="text" name="copyto" size="50" value="<?php echo $data['copyto']; ?>" data-enter-submit="true">
                <?php echo $LANG['enteremailaddressesseparatedcomma'];?> 
            </td>
        </tr>
    </table>
<!--    <div style="float:right;">
        <input type="submit" name="toggleeditor" value="Enable/Disable Rich-Text Editor" class="btn btn-sm">
    </div>-->
    <br>
    <?php echo $LANG['subject'];?> 
    <input type="text" name="subject" size="80" value="<?php echo $data['subject']; ?>" data-enter-submit="true">
    <br><br>
    <textarea name="message" class="tinymce" id="email_msg1"><?php echo $data['message']; ?></textarea>
    <br>
    <div class="btn-container">
        <input type="submit" id="savechanges" name="savechanges" value="Save Changes" class="btn btn-primary">
        <input type="button" value="Cancel Changes" onclick="window.location='addonmodules.php?module=accountStatement'" class="btn btn-default">
    </div>
</form>

<script type="text/javascript" src="<?php echo $baseurl; ?>/assets/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var baseurl = '<?php echo $baseurl; ?>';
        $("textarea.tinymce").tinymce({
            // Location of TinyMCE script
            script_url : baseurl+"/assets/js/tiny_mce/tiny_mce.js",

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,advlist",
            entity_encoding : "raw",

            // Theme options
            theme_advanced_buttons1 : "fontselect,fontsizeselect,forecolor,backcolor,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            convert_urls : false,
            relative_urls : false,
            forced_root_block : false,
            width: '100%',
            height: '300px'
        });
    });

    var editorEnabled = true;

    function toggleEditor() {
        if (editorEnabled == true) {
            tinymce.EditorManager.execCommand("mceRemoveControl", true, "email_msg1");
            editorEnabled = false;
        } else {
            tinymce.EditorManager.execCommand("mceAddControl", true, "email_msg1");
            editorEnabled = true;
        }
    }

    function insertMergeField(mfield) {
        $("#email_msg1").tinymce().execCommand("mceInsertContent",false,'{$'+mfield+'}');
    }

</script>
<h2 id="mergefieldstoggle"><?php echo $LANG['availablemergefields'];?></h2>
<div id="mergefields" style="border:1px solid #8FBCE9;background:#ffffff;color:#000000;padding:5px;height:300px;overflow:auto;font-size:10px;z-index:10;">

<table width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td width="50%" valign="top">

<b><?php echo $LANG['clientrelated'];?></b><br>
<table>
    <tbody>
        <tr>
            <td width="150"><a href="#" onclick="insertMergeField('client_id');return false">ID</a></td>
            <td>{$client_id}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_name');return false">Client Name</a></td>
            <td>{$client_name}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_first_name');return false">First Name</a></td>
            <td>{$client_first_name}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_last_name');return false">Last Name</a></td>
            <td>{$client_last_name}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_company_name');return false">Company Name</a></td>
            <td>{$client_company_name}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_email');return false">Email Address</a></td>
            <td>{$client_email}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_address1');return false">Address 1</a></td>
            <td>{$client_address1}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_address2');return false">Address 2</a></td>
            <td>{$client_address2}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_city');return false">City</a></td>
            <td>{$client_city}</td>
        </tr>
        `   <tr>
            <td><a href="#" onclick="insertMergeField('client_state');return false">State/Region</a></td>
            <td>{$client_state}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_postcode');return false">Postcode</a></td>
            <td>{$client_postcode}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_country');return false">Country</a></td>
            <td>{$client_country}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_phonenumber');return false">Phone Number</a></td>
            <td>{$client_phonenumber}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_password');return false">Password</a></td>
            <td>{$client_password}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_signup_date');return false">Signup Date </a></td>
            <td>{$client_signup_date}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_due_invoices_balance');return false">Total Due Invoices Balance </a></td>
            <td>{$client_due_invoices_balance}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('client_status');return false">Status</a></td>
            <td>{$client_status}</td>
        </tr>
    </tbody>
</table>
<br>
<b><?php echo $LANG['other'];?></b><br>
<table>
    
    <tbody>
        <tr>
            <td width="150"><a href="#" onclick="insertMergeField('company_name');return false">Company Name</a></td>
            <td>{$company_name}</td>
        </tr>
        <tr>
            <td width="150"><a href="#" onclick="insertMergeField('company_domain');return false">Domain</a></td>
            <td>{$company_domain}</td>
        </tr>
        <tr>
            <td width="150"><a href="#" onclick="insertMergeField('company_logo_url');return false">Logo URL</a></td>
            <td>{$company_logo_url}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('whmcs_url');return false">WHMCS URL</a></td>
            <td>{$whmcs_url}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('whmcs_link');return false">WHMCS Link</a></td>
            <td>{$whmcs_link}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('unsubscribe_url');return false">Marketing Unsubscribe URL</a></td>
            <td>{$unsubscribe_url}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('signature');return false">Signature</a></td>
            <td>{$signature}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('date');return false">Full Sending Date</a></td>
            <td>{$date}</td>
        </tr>
        <tr>
            <td><a href="#" onclick="insertMergeField('time');return false">Full Sending Time</a></td>
            <td>{$time}</td>
        </tr>
    </tbody>
</table>
</td></tr></tbody></table>
</div>
