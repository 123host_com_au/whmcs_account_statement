<?php

# tcpdf Font
$pdffont = $accountsummary->pdfFont();
$getPdfFont = $accountsummary->getPdfFont();
# tcpdf Page Size
$pdfpagesize = $accountsummary->pdfPaperSize();

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
if ($getPdfFont == 'custom' && !empty($pdffont)) {
    $pdffont = TCPDF_FONTS::addTTFfont(__DIR__ . '/font/' . $pdffont, 'TrueTypeUnicode', '', 32);
}

$pdf->SetPrintHeader(false);
$pdf->AddPage("P", strtoupper($pdfpagesize));



# Logo
$logoFilename = 'placeholder.png';
if (file_exists(ROOTDIR . '/assets/img/logo.png')) {
    $logoFilename = 'logo.png';
} elseif (file_exists(ROOTDIR . '/assets/img/logo.jpg')) {
    $logoFilename = 'logo.jpg';
}

$pdf->Image(ROOTDIR . '/assets/img/' . $logoFilename, 15, 25, 75);

# Company Details
$pdf->SetXY(15, 42);
$pdf->SetFont($pdffont, '', 13);
$comapnyAddress = $accountsummary->get_companyAddress();
foreach ($comapnyAddress as $address) {
    $pdf->Cell(180, 4, trim($address), 0, 1, 'R');
    $pdf->SetFont($pdffont, '', 9);
}
$pdf->Ln(5);

# Clients Details
$addressypos = $pdf->GetY();
$pdf->SetFillColor(255);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->Cell(0, 0, 'Date:    ' . date('d/m/Y'), 0, 1, 'L', '1');
$pdf->Ln(2);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->Cell(0, 4, Lang::trans('invoicesinvoicedto'), 0, 1);
$pdf->SetFont($pdffont, '', 9);
$clientsdetails = $accountsummary->get_clientdetails($userid);
if ($clientsdetails["companyname"]) {
    $pdf->Cell(0, 4, $clientsdetails["companyname"], 0, 1, 'L');
    $pdf->Cell(0, 4, Lang::trans('invoicesattn') . ': ' . $clientsdetails["firstname"] . ' ' . $clientsdetails["lastname"], 0, 1, 'L');
} else {
    $pdf->Cell(0, 4, $clientsdetails["firstname"] . " " . $clientsdetails["lastname"], 0, 1, 'L');
}
$pdf->Cell(0, 4, $clientsdetails["address1"], 0, 1, 'L');
if ($clientsdetails["address2"]) {
    $pdf->Cell(0, 4, $clientsdetails["address2"], 0, 1, 'L');
}
$pdf->Cell(0, 4, $clientsdetails["city"] . ", " . $clientsdetails["state"] . ", " . $clientsdetails["postcode"], 0, 1, 'L');
$pdf->Cell(0, 4, $clientsdetails["country"], 0, 1, 'L');

$pdf->Ln(7);
$pdf->SetFillColor(255);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->SetTextColor(0);
$pdf->Cell(0, 0, 'INVOICE DETAILS', 0, 1, 'L', '1');
$pdf->SetTextColor(0);
$pdf->Ln(2);
$pdf->SetFont($pdffont, '', 9);
$tblhtml = '<table width="100%" bgcolor="#ccc" cellspacing="1" cellpadding="2" border="0">
                <tr height="30" bgcolor="#efefef" style="font-weight:bold;text-align:center;">
                    <th width="5%">SNO.</th>
                    <th width="20%">Invoice No</th>
                    <th width="25%">Invoice Date</th>
                    <th width="25%">Amount</th>
                    <th width="25%">Balance</th>
                </tr>';
$totalBal = (float) 0;
$totalAmonutSum = (float) 0;
$count = 0;
foreach ($dueinvoices as $id => $invoice) {

    if ($invoice["number"] == "") {
        $invoicenumber = $id;
    } else {
        $invoicenumber = $invoice["number"];
    }
    $invoicetitle = "Invoice #" . $id;
    $totalAmount = $accountsummary->invoice_amount($id);
    $transactions = $accountsummary->invoice_transaction_details($id, $userid);
    $count++;
    $ptotalAmount = 0;
    $ptotalcredit = 0;
    $pbalance = 0;
    if (preg_match('/c/', $totalAmount)) {
        $totalAmount = explode("*", $totalAmount);
        $totalAmount = $totalAmount[0];
        $balance = 0;
    } elseif(preg_match('/p/', $totalAmount)) {
        $totalAmount = explode("*p", $totalAmount);
        $ptotalAmount = $totalAmount[0];
        $ptotalcredit = $totalAmount[1];
        $pbalance = $ptotalAmount + $ptotalcredit;
        $balance = $ptotalAmount;
        $totalAmount = $ptotalAmount + $ptotalcredit;
    } else {
        $paidtotal = (float) 0;
        $refundtotal = (float) 0;
        foreach ($transactions AS $trans) {
            $paidtotal = $paidtotal + $trans['amountin'];
            $refundtotal = $refundtotal + $trans['amountout'];
        }
        $balance = $totalAmount - ($paidtotal - $refundtotal);
    }
    $totalamountstring = $accountsummary->customFormatCurrency($totalAmount, $userid);
    $totalbalancestring = $accountsummary->customFormatCurrency(number_format((float) round($balance, 2), 2, '.', ''), $userid);
    
    // Calculation for partial paid invoice
    if( $ptotalAmount > 0 && $ptotalcredit > 0 && $pbalance > 0 ) {
        $ptotalamountstring = $accountsummary->customFormatCurrency(number_format((float) round($ptotalAmount, 2), 2, '.', ''), $userid);
        $ptotalcreditstring = $accountsummary->pcustomFormatCurrency(number_format((float) round($ptotalcredit, 2), 2, '.', ''), $userid);
        $ptotalbalancestring = $accountsummary->pcustomFormatCurrency(number_format((float) round($pbalance, 2), 2, '.', ''), $userid);
        $totalamountstring = $accountsummary->customFormatCurrency(number_format((float) round($pbalance, 2), 2, '.', ''), $userid);
        $totalbalancestring = $ptotalbalancestring ." - ". $ptotalcreditstring ." = ". $ptotalamountstring;
    }
    if (is_array($invoice["masspay"]) && count($invoice["masspay"]) > 0) {
        $balance = 0.00;
        $totalAmount = 0.00;
        $invoicenumber = $invoicenumber . " (Invoices:" . implode(",", $invoice["masspay"]) . ")";
        $totalamountstring = $totalamountstring . " (MassPaid)";
        $totalbalancestring = $totalbalancestring . " (MassPaid)";
    }
    $tblhtml .= '<tr bgcolor="#fff">
                    <td align="center">' . $count . '</td>
                    <td align="center">' . $invoicenumber . '</td>
                    <td align="center">' . date("d-M-Y", strtotime($invoice['create'])) . '</td>
                    <td align="center">' . $totalamountstring . '</td>
                    <td align="center">' . $totalbalancestring . '</td>
                </tr>';
    $totalBal = $totalBal + $balance;
    $totalAmonutSum = $totalAmonutSum + $totalAmount;
}
$tblhtml .= '<tr height="30" bgcolor="#efefef" style="font-weight:bold;">
                <td colspan="3" align="right">Total:</td>
                <td align="center">' . $accountsummary->customFormatCurrency(number_format((float) round($totalAmonutSum, 2), 2, '.', ''), $userid) . '</td>
                <td align="center">' . $accountsummary->customFormatCurrency(number_format((float) round($totalBal, 2), 2, '.', ''), $userid) . '</td>
            </tr>
        </table>';
$pdf->writeHTML($tblhtml, true, false, false, false, '');

$pdf->Ln(7);
$pdf->SetFillColor(255);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->SetTextColor(0);
$pdf->Cell(0, 0, 'PAYMENT DETAILS', 0, 1, 'L', '1');
$pdf->SetTextColor(0);
$pdf->Ln(2);
$pdf->SetFont($pdffont, '', 9);
$tblpaymenthtml = '<table width="100%" bgcolor="#ccc" cellspacing="1" cellpadding="2" border="0">
                <tr height="30" bgcolor="#efefef" style="font-weight:bold;text-align:center;">
                    <th width="5%">SNo.</th>
                    <th width="15%">Invoice No</th>
                    <th width="30%">Transaction ID</th>
                    <th width="30%">Payment Date</th>
                    <th width="20%">Paid Amount</th>
                </tr>';


$transactionsArr = array();
foreach ($dueinvoices as $invoiceid => $invoice) {
    if ($invoice["number"] == "") {
        $invoicenumber = $invoiceid;
    } else {
        $invoicenumber = $invoice["number"];
    }
    $transactionslist = $accountsummary->invoicetransctions($invoiceid, $userid);
    if (!empty($transactionslist)) {
        if (is_array($invoice["masspay"]) && count($invoice["masspay"]) > 0) {
            $length = count($transactionslist);
            $transactionslist[$length - 1]['masspay'] = implode(", ", $invoice["masspay"]);
        }
        $transactionsArr[$invoicenumber] = $transactionslist;
    }
}

$transcount = 0;
$transactionAmount = (float) 0;
if (!empty($transactionsArr)) {
    foreach ($transactionsArr as $invoiceid => $transactiondetailArr) {
        foreach ($transactiondetailArr as $transactiondetail) {
            $transcount++;
            $refund = '';
            if ($transactiondetail['refundid'] != '0') {
                $refund = ' (Refunded)';
            }
            $amountinstring = $accountsummary->customFormatCurrency(number_format((float) round($transactiondetail['amountin'], 2), 2, '.', ''), $userid);
            $amountin = number_format((float) round($transactiondetail['amountin'], 2), 2, '.', '');
            if (!empty($transactiondetail["masspay"])) {
                $amountinstring = $amountinstring." (MassPaid)";
                $amountin = 0.00;
                $invoiceid = $invoiceid . " (Invoices:" . $transactiondetail["masspay"] . ")";
            }
            $tblpaymenthtml .= '<tr bgcolor="#fff">
                                <td align="center">' . $transcount . '</td>
				<td align="center">' . $invoiceid . '</td>
                                <td align="center">' . $transactiondetail['transid'] . $refund . '</td>    
                                <td align="center">' . date("d-M-Y", strtotime($transactiondetail['date'])) . '</td>    
                                <td align="center">' . $amountinstring . '</td>    
                            </tr>';
            $transactionAmount = $transactionAmount + $amountin;
        }
    }
} else {
    $tblpaymenthtml .= '<tr bgcolor="#fff"><td colspan="5" align="center">No Related Transactions Found</td></tr>';
}
$tblpaymenthtml .= '<tr height="30" bgcolor="#efefef" style="font-weight:bold;">
                <td colspan="4" align="right">Total Payment:</td>
                <td align="center">' . $accountsummary->customFormatCurrency(number_format((float) round($transactionAmount, 2), 2, '.', ''), $userid) . '</td>
            </tr>
        </table>';
$pdf->writeHTML($tblpaymenthtml, true, false, false, false, '');

$pdf->Ln(7);
$pdf->SetFillColor(255);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->SetTextColor(0);
$pdf->Cell(180, 0, 'Total Invoice Amount:    ' . $accountsummary->customFormatCurrency(number_format((float) round($totalAmonutSum, 2), 2, '.', ''), $userid), 0, 1, 'R', '1');

$pdf->Ln(1);
$pdf->SetFillColor(255);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->SetTextColor(0);
$pdf->Cell(180, 0, 'Total Payment Made / Credit Applied:     ' . $accountsummary->customFormatCurrency(number_format((float) round($transactionAmount, 2), 2, '.', ''), $userid), 0, 1, 'R', '1');

$pdf->Ln(1);
$pdf->SetFillColor(255);
$pdf->SetFont($pdffont, 'B', 10);
$pdf->SetTextColor(0);
$pdf->Cell(180, 0, 'Net Amount Payable:    ' . $accountsummary->customFormatCurrency(number_format((float) round($totalBal, 2), 2, '.', ''), $userid), 0, 1, 'R', '1');

$pdf->SetTextColor(0);
$pdf->Ln(5);
$pdf->SetFont($pdffont, '', 9);

$accountsummary->remove_attachments($userid);

$log_filename = 'Account_Summary_' . $userid . '_' . date('Y-m-d_h-i-s') . '.pdf';

$path_log = __DIR__ . '/attachments/' . $log_filename;
$lnk = fopen($path_log, "w");
$aa = $pdf->Output($path_log, 'F');
//$aa = $pdf->Output($path_log, 'I');
if (!$aa) {
    $accountsummary->add_attachments($userid, $log_filename);
    $temp = true;
} else {
    $temp = false;
}
fwrite($lnk, ob_get_clean());
fclose($lnk);