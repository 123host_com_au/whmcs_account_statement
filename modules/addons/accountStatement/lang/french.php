<?php

# Account Statement addon language variable
$_ADDONLANG['modulename'] = "État de Compte";
$_ADDONLANG['linktemplateclients']='Ajoutez ce lien dans votre modèle pour afficher les clients.';
$_ADDONLANG['pdfpapersize']='Format du PDF';
$_ADDONLANG['pdfletter']='Lettre';
$_ADDONLANG['choosegeneratingfiles']='Choisissez le format papier utilisé pour les impressions PDF';
$_ADDONLANG['pdffontfamily']='Choix de Police PDF';
$_ADDONLANG['invoicetype']='Type de Facture';
$_ADDONLANG['invoicesunpaid']='Impayées';
$_ADDONLANG['invoicespaid']='Payées';
$_ADDONLANG['invoicesall']='Toute';
$_ADDONLANG['chooseinvoicegeneratingfiles']='Choisissez le type de facture à utiliser lors de la génération de PDF';
$_ADDONLANG['enableinvoices']='Activez l\'envois PDF par courriel';
$_ADDONLANG['from']='De';
$_ADDONLANG['copyto']='Copie à';
$_ADDONLANG['enteremailaddressesseparatedcomma']='Inscrivez les courriels des destinataires.  Ils doivent être séparées par une virgule';
$_ADDONLANG['subject']='Sujet:';
$_ADDONLANG['availablemergefields']='Merge Fields Disponibles';
$_ADDONLANG['clientrelated']='Associé au Client';
$_ADDONLANG['other']='Autre';
$_ADDONLANG['accountstatement']='État de Compte';
$_ADDONLANG['home']='Accueil';
$_ADDONLANG['emailtemplate']='Modèle des Courriels';
$_ADDONLANG["enableautomatic"]='Active les envois mensuels';
$_ADDONLANG['ttsmonthlystatement']='Cochez pour automatiser les envois mensuels';
$_ADDONLANG['ttspdf']='Cochez pour envoyer une copie PDF avec le courriel';
$_ADDONLANG["includeallpaidinvoices"]='Inclure TOUTE facture impayée';
$_ADDONLANG["includeallpaidinvoicesdes"] = 'Si choisi, l\'intégralité des impayés sera inclus dans l\'État de Compte.<br/>
        Autrement, seul les impayés comprises entre les dates choisies seront incluses.';
