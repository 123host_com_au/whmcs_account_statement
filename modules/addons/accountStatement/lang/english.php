<?php

# Account Statement addon language variable
$_ADDONLANG['modulename'] = "Account Statement";
$_ADDONLANG['linktemplateclients']='Add this link in your template to view clients';
$_ADDONLANG['pdfpapersize']='PDF Paper Size';
$_ADDONLANG['pdfletter']='letter';
$_ADDONLANG['choosegeneratingfiles']='Choose the paper format to use when generating PDF files';
$_ADDONLANG['pdffontfamily']='PDF Font Family';
$_ADDONLANG['invoicetype']='Invoice Type';
$_ADDONLANG['invoicesunpaid']='Unpaid';
$_ADDONLANG['invoicespaid']='Paid';
$_ADDONLANG['invoicesall']='All';
$_ADDONLANG['chooseinvoicegeneratingfiles']='Choose the invoice type to use when generating PDF files';
$_ADDONLANG['enableinvoices']='Enable PDF Invoices';
$_ADDONLANG['from']='From';
$_ADDONLANG['copyto']='Copy To';
$_ADDONLANG['enteremailaddressesseparatedcomma']='Enter email addresses separated by a comma';
$_ADDONLANG['subject']='Subject:';
$_ADDONLANG['availablemergefields']='Available Merge Fields';
$_ADDONLANG['clientrelated']='Client Related';
$_ADDONLANG['other']='Other';
$_ADDONLANG['accountstatement']='Account Statement';
$_ADDONLANG['home']='Home';
$_ADDONLANG['emailtemplate']='Email Template';
$_ADDONLANG["enableautomatic"]='Enable Auto Monthly Statement';
$_ADDONLANG['ttsmonthlystatement']='Tick to send automatic monthly statements';
$_ADDONLANG['ttspdf']='Tick to send PDF invoice statements along with invoice emails';
$_ADDONLANG["includeallpaidinvoices"]='Include ALL unpaid invoices';
$_ADDONLANG["includeallpaidinvoicesdes"] = 'If this is checked, then ALL UNPAID invoices will be included on the statement.<br/>
        If it is NOT checked, then only invoices from the last month or selected period will be included.';
